//
//  PhotoModel.h
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PhotoModel : NSObject

@property (nonatomic, readwrite) long long photoID;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *owner;
@property (nonatomic, copy) NSString *farm;
@property (nonatomic, copy) NSString *secret;
@property (nonatomic, copy) NSString *server;
@property (nonatomic, readonly) NSString *url;
@property (nonatomic, readonly) NSString *thumbUrl;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end