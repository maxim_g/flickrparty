//
//  PhotoModel.m
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "PhotoModel.h"

@implementation PhotoModel

- (instancetype)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    if(self)
    {
        _photoID = [dictionary[@"id"] longLongValue];
        _owner = dictionary[@"owner"];
        _title = dictionary[@"title"] ? dictionary[@"title"] : @"noname";
        _farm = dictionary[@"farm"];
        _secret = dictionary[@"secret"];
        _server = dictionary[@"server"];
    }
    
    return self;
}

#pragma mark - Getters

- (NSString *)url
{
    return [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%lli_%@_b.jpg", _farm, _server, _photoID, _secret];
}

- (NSString *)thumbUrl
{
    return [NSString stringWithFormat:@"http://farm%@.static.flickr.com/%@/%lli_%@_s.jpg", _farm, _server, _photoID, _secret];
}

@end
