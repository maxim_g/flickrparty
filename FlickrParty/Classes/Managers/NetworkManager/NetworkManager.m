//
//  NetworkManager.m
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "NetworkManager.h"
#import <Reachability/Reachability.h>
#import "ErrorHandler.h"

NSString * const kHost = @"https://api.flickr.com/services/rest/?";
NSString * const kApiKey = @"5dc0aedd5a7ea13aa437d71ffb831318";
NSString * const kSearchMethod = @"flickr.photos.search";

@implementation NetworkManager

NSString *combineUrlWithParams(NSString *url, NSDictionary *params)
{
    NSString *result = url;
    for(NSString *key in params.allKeys)
    {
        result = [result stringByAppendingFormat:@"%@=%@", key, params[key]];
        result = [result stringByAppendingString:@"&"];
    }
    //to remove last unnecessary '&' symbol
    if(params.count)
        result = [result substringToIndex:result.length - 1];
    
    return result;
}

#pragma mark - Public Methods

+ (void)searchPhotosWithTag:(NSString *)tag
                       page:(NSInteger)page
                 itemsCount:(NSInteger)itemsCount
                 completion:(CompletionHandler)completion
{
    NSError *error = nil;
    [self checkInternetConnection:&error];
    
    if(!tag)
        error = [NSError errorWithDomain:@"Internal Error" code:InternalError userInfo:nil];
    
    if(error)
    {
        if(completion)
            completion(nil, error);
        
        return;
    }
        
    if(itemsCount <= 0) itemsCount = kPerPage;
    NSDictionary *params = @{@"method"         : kSearchMethod,
                             @"api_key"        : kApiKey,
                             @"tags"           : tag,
                             @"page"           : @(page).stringValue,
                             @"format"         : @"json",
                             @"nojsoncallback" : @"1",
                             @"per_page"       : @(itemsCount).stringValue};
    
    
    NSURL *url = [NSURL URLWithString:[combineUrlWithParams(kHost, params) stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    [self getDataFromURL:url completion:completion];
}

#pragma mark - Stuff

+ (void)checkInternetConnection:(NSError **)error
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    if(!reachability.isReachable)
        *error = [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorNotConnectedToInternet userInfo:nil];
}

#warning For best practice should be used AFNetworking (Unfortunately don't have more time for this)
+ (void)getDataFromURL:(NSURL *)url completion:(CompletionHandler)completion
{
    if(!url)
        if(completion) return completion(nil, [NSError errorWithDomain:NSURLErrorDomain code:NSURLErrorBadURL userInfo:nil]);
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSError *error = nil;
        NSDictionary *result = nil;
        
        NSData *data = [NSData dataWithContentsOfURL:url options:0 error:&error];
        if(!error)
            result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
        
        dispatch_async(dispatch_get_main_queue(), ^{
            if(completion)
                completion(result, error);
        });
    });
}

@end
