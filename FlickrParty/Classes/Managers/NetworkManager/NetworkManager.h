//
//  NetworkManager.h
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <Foundation/Foundation.h>

NSInteger const kPerPage = 30;

typedef void(^CompletionHandler)(NSDictionary *response, NSError *error);

@interface NetworkManager : NSObject

/*!
 @method searchPhotosWithTag:page:itemsCount:completion:
 @discussion getPhotos by Tag with defined page and items count per page
 @param tag - the tag used for search
 @param page - the page number
 @param itemsCount - items count per page, if value is 0 or less then the default value kPerPage = 30 is used
 @param completion - block that is invoked after all the stuff is finished
 */

+ (void)searchPhotosWithTag:(NSString *)tag
                       page:(NSInteger)page
                 itemsCount:(NSInteger)itemsCount
                 completion:(CompletionHandler)completion;

@end
