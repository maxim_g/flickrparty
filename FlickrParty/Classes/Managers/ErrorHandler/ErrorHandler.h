//
//  ErrorHandler.h
//  FlickrParty
//
//  Created by Max on 24.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ENUM(NSInteger)
{
    InternalError = 100,
};

@interface ErrorHandler : NSObject

+ (NSString *)getMessageForError:(NSError *)error;

@end
