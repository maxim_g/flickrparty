//
//  ErrorHandler.m
//  FlickrParty
//
//  Created by Max on 24.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "ErrorHandler.h"

@implementation ErrorHandler

+ (NSString *)getMessageForError:(NSError *)error
{
    switch (error.code) {
        case InternalError:
        case NSURLErrorBadURL:                 return NSLocalizedString(@"Internal Error. Please contact dev team.", nil);

        case NSURLErrorNotConnectedToInternet: return NSLocalizedString(@"Please check your internet connection and try again", nil);
            
        default:                               return NSLocalizedString(@"Smth went wrong", nil);
    }
    
    return NSLocalizedString(@"Smth went wrong", nil);
}

@end
