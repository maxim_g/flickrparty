//
//  AlertHelper.h
//  FlickrParty
//
//  Created by Max on 24.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AlertHelper : NSObject

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message;

- (void)show;

@end
