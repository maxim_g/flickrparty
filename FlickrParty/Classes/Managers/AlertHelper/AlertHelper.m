//
//  AlertHelper.m
//  FlickrParty
//
//  Created by Max on 24.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "AlertHelper.h"

@interface AlertHelper ()
@property (nonatomic, strong) NSString *title;
@property (nonatomic, strong) NSString *message;
@end

@implementation AlertHelper

#pragma mark - Public Methods

- (instancetype)initWithTitle:(NSString *)title
                      message:(NSString *)message
{
    self = [super init];
    
    if(self)
    {
        self.title = title;
        self.message = message;
    }
    
    return self;
}

- (void)show
{
    if ([UIAlertController class])
        [self initializeAlertController];
        
    else
        [self initializeAlertView];
}

#pragma mark - Stuff

- (void)initializeAlertController
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:self.title
                                                                             message:self.message
                                                                      preferredStyle:UIAlertControllerStyleAlert];
    [alertController addAction:[self cancelAction]];
    
    [[self topMostController] presentViewController:alertController animated:YES completion:nil];
}

- (void)initializeAlertView
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:self.title
                                                    message:self.message
                                                   delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", nil)
                                          otherButtonTitles:nil];
    [alert show];
}

- (UIAlertAction *)cancelAction
{
    UIAlertAction *action = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", nil)
                                                     style:UIAlertActionStyleDefault
                                                   handler:^(UIAlertAction *action) {
                                   
                               }];
    
    return action;
}

- (UIViewController *)topMostController
{
    UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
    
    while (topController.presentedViewController)
        topController = topController.presentedViewController;
    
    return topController;
}

@end
