//
//  MainViewController.m
//  FlickrParty
//
//  Created by Max on 13.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "MainViewController.h"
#import <MWPhotoBrowser/MWPhotoBrowser.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "NetworkManager.h"
#import "PhotoModel.h"
#import "PreviewTableViewCell.h"
#import "ErrorHandler.h"

NSString * const kTag = @"Party";

@interface MainViewController () <UITableViewDataSource, UITableViewDelegate, MWPhotoBrowserDelegate>
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UIRefreshControl *refreshControl;

@property (nonatomic, copy) NSArray *images;
@property (nonatomic, assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL isLoading;
@end

@implementation MainViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = kTag;
    self.currentPage = 1;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self configureMessageLabel];
    [self configureTableView];
    [self preloadData];
}

#pragma mark - Setters

- (void)setIsLoading:(BOOL)isLoading
{
    if(_isLoading == isLoading)
        return;
    
    _isLoading = isLoading;
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:isLoading];
}

#pragma mark - Stuff

- (void)preloadData
{
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [self getPhotosWithPage:self.currentPage
                 itemsCount:kPerPage
              shouldReplace:YES
                 completion:^{
                     [MBProgressHUD hideHUDForView:self.view animated:YES];
                 }];
}

- (void)getPhotosWithPage:(NSInteger)page
               itemsCount:(NSInteger)itemsCount
            shouldReplace:(BOOL)shouldReplace
               completion:(void(^)(void))completion
{
    if(self.isLoading) return;
    
    self.isLoading = YES;

    [NetworkManager searchPhotosWithTag:kTag
                                   page:page
                             itemsCount:itemsCount
                             completion:^(NSDictionary *response, NSError *error) {
        [self handleResponse:response error:error shouldReplace:shouldReplace];
        if(completion) completion();
                                 
        self.isLoading = NO;
    }];
}

- (void)handleResponse:(NSDictionary *)response error:(NSError *)error shouldReplace:(BOOL)shouldReplace
{
    if(!response || error)
    {
        NSString *message = @"Smth went wrong, please try again";
        if(error)
            message = [ErrorHandler getMessageForError:error];
        
        [self showMessage:message];
        
        //return page index to previous value if there was some error while loading a chunk
        if(self.currentPage > 1) self.currentPage--;
        return;
    }
    
    NSArray *photos = [response valueForKeyPath:@"photos.photo"];
    if(!photos.count)
    {
        NSString *message = NSLocalizedString(@"Nothing was found by tag", nil);
        [self showMessage:[message stringByAppendingFormat:@" %@", kTag]];
        return;
    }
    
    NSMutableArray *items = @[].mutableCopy;
    if(!shouldReplace)
        [items addObjectsFromArray:self.images];
    
    for(NSDictionary *photo in photos)
    {
        PhotoModel *model = [[PhotoModel alloc] initWithDictionary:photo];
        [items addObject:model];
    }
    
    self.images = items;
    [self.tableView reloadData];
}

- (void)showMessage:(NSString *)message
{
    self.messageLabel.text = message;
    
    AlertHelper *alert = [[AlertHelper alloc] initWithTitle:NSLocalizedString(@"Error", nil) message:message];
    [alert show];
}

- (void)configureMessageLabel
{
    CGFloat offset = 20.f;
    CGRect rect = self.view.bounds;
    rect.origin.x = offset;
    rect.size.width -= offset * 2;
    
    self.messageLabel = [[UILabel alloc] initWithFrame:rect];
    self.messageLabel.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.messageLabel.textColor = [UIColor lightGrayColor];
    self.messageLabel.textAlignment = NSTextAlignmentCenter;
    self.messageLabel.numberOfLines = 0;
    self.messageLabel.backgroundColor = [UIColor clearColor];
    self.messageLabel.font = [UIFont systemFontOfSize:21];
    [self.view addSubview:self.messageLabel];
}

- (void)configureTableView
{
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.tableView];
 
    [self prepareRefreshControl];
    
    //remove extra lines
    self.tableView.tableFooterView = [UIView new];
}

- (void)prepareRefreshControl
{
    self.refreshControl = [[UIRefreshControl alloc] init];
    [self.refreshControl addTarget:self action:@selector(refreshContent) forControlEvents:UIControlEventValueChanged];
    [self.tableView addSubview:self.refreshControl];
}

- (void)refreshContent
{
    [self getPhotosWithPage:1
                 itemsCount:self.images.count
              shouldReplace:YES
                 completion:^{
        [self.refreshControl endRefreshing];
    }];
}

- (void)showGalleryWithPhotoIndex:(NSInteger)photoIndex
{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    browser.zoomPhotosToFill = YES;
    [browser setCurrentPhotoIndex:photoIndex];
    
    UINavigationController *nc = [[UINavigationController alloc] initWithRootViewController:browser];
    nc.modalTransitionStyle = UIModalTransitionStyleCrossDissolve;
    nc.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - UITableView DataSource & Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.images.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 100.f;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    PreviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if(!cell)
        cell = [[PreviewTableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:CellIdentifier];
    
    [cell configureWithPhoto:self.images[indexPath.row]];
    
    return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self showGalleryWithPhotoIndex:indexPath.row];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    //start load new chunk when left 5 rows to scroll to the end of the table
    if(indexPath.row == self.images.count - 5)
    {
        [self getPhotosWithPage:++self.currentPage
                     itemsCount:kPerPage
                  shouldReplace:NO
                     completion:nil];
    }
}

#pragma mark - MWPhotoBrowser Delegate

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser
{
    return self.images.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index
{
    if (index < self.images.count)
    {
        PhotoModel *photoModel = self.images[index];
        MWPhoto *photo = [MWPhoto photoWithURL:[NSURL URLWithString:photoModel.url]];
        photo.caption = photoModel.title;
    
        return photo;
    }
    
    return nil;
}

@end
