//
//  PreviewTableViewCell.h
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import <UIKit/UIKit.h>

@class PhotoModel;

@interface PreviewTableViewCell : UITableViewCell

- (void)configureWithPhoto:(PhotoModel *)photo;

@end
