//
//  PreviewTableViewCell.m
//  FlickrParty
//
//  Created by Max on 16.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "PreviewTableViewCell.h"
#import "PhotoModel.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface PreviewTableViewCell ()
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;
@end

@implementation PreviewTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if(self)
    {
        [self prepareUI];
    }
    
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    self.activityIndicator.center = self.imageView.center;
}

- (void)prepareForReuse
{
    [self.activityIndicator stopAnimating];
    self.imageView.image = nil;
}

#pragma mark - Stuff

- (void)prepareUI
{
    self.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    self.textLabel.numberOfLines = 4;
    
    self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicator.hidesWhenStopped = YES;
    self.activityIndicator.frame = CGRectMake(0, 0, 30, 30);
    [self addSubview:self.activityIndicator];
}

#pragma mark - Public Methods

- (void)configureWithPhoto:(PhotoModel *)photo
{
    self.textLabel.text = photo.title;
    
    [self.activityIndicator startAnimating];
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:photo.thumbUrl]
                      placeholderImage:[UIImage imageNamed:@"placeholder"]
                             completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL) {
        //update cell after image is loaded
        [self setNeedsLayout];
        [self.activityIndicator stopAnimating];
    }];
}

@end
