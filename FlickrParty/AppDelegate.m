//
//  AppDelegate.m
//  FlickrParty
//
//  Created by Max on 13.06.16.
//  Copyright © 2016 Max G. All rights reserved.
//

#import "AppDelegate.h"
#import "MainViewController.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    UINavigationController *navVc = [[UINavigationController alloc] initWithRootViewController:[MainViewController new]];
    self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.rootViewController = navVc;
    [self.window makeKeyAndVisible];
    
    return YES;
}

@end
